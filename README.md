# drupal-base docker image

Base image containing all dependencies of Drupal.

## Contains

### Base images

- PHP-FPM on port 9000
- PHP Extensions
- nginx on port 80
- `/scripts/serve.sh` starts the Nginx and PHP-FPM daemons, both in the background.
- `/scripts/serve-fg.sh` starts the Nginx and PHP-FPM daemons, with nginx running in the foreground. If you are
  extending this image, you might want to use this as your `CMD`.
- `/scripts/up.sh` checks if Drupal is bootstrapped, and if so, updates the drupal database and imports config.

### Development images

- composer
- unzip
- git
- openssh-client
- runuser
- xdebug
- bash

## Does not contain

By design, these images do not contain the Drupal source code.
You should mount your own Drupal project at `/drupal`.
This project should contain a `web` and `vendor` folder.

## How to ...

### Build and run locally

Minimal build:

```shell
docker build --tag refstudycentre/drupal-base:latest --target base .
```

Build with extra development utilities:
```shell
docker build --tag refstudycentre/drupal-base:dev-latest --target dev .
```

Run a container:
```shell
docker run -p 80:80 --rm --name drupal-base refstudycentre/drupal-base:latest
```

### Update on hub.docker.com

See [bitbucket-pipelines.yml](bitbucket-pipelines.yml)
