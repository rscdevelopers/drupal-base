#!/usr/bin/env sh

set -eu

# Check if Drupal is bootstrapped before trying to deploy.
# See https://drupal.stackexchange.com/a/249197/8452
SUCCESSFUL="$(drush php-eval 'if (function_exists("t")) echo t("Successful");')"
if drush status bootstrap | grep -q "$SUCCESSFUL"
then
  # See https://www.drush.org/latest/deploycommand/
  drush deploy -v -y
else
  echo "Cannot upgrade. Drupal is not bootstrapped."
  exit 1
fi
