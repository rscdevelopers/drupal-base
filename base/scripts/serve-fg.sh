#!/usr/bin/env sh

set -eu

echo "Starting PHP-FPM …"
php-fpm --daemonize

echo "Starting Nginx …"
nginx -g "daemon off;"
